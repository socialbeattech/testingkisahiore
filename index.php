<?php 

$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

?> 
<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
<title>	Shriram Sensible Soch</title>
<meta name="description" content="Shriram Properties Sensible Soch">
<style>:root{--blue:#007bff;--indigo:#6610f2;--purple:#6f42c1;--pink:#e83e8c;--red:#dc3545;--orange:#fd7e14;--yellow:#ffc107;--green:#28a745;--teal:#20c997;--cyan:#17a2b8;--white:#fff;--gray:#868e96;--gray-dark:#343a40;--primary:#007bff;--secondary:#868e96;--success:#28a745;--info:#17a2b8;--warning:#ffc107;--danger:#dc3545;--light:#f8f9fa;--dark:#343a40;--breakpoint-xs:0;--breakpoint-sm:576px;--breakpoint-md:768px;--breakpoint-lg:992px;--breakpoint-xl:1200px;--font-family-sans-serif:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";--font-family-monospace:"SFMono-Regular",Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace}*,::after,::before{box-sizing:border-box}html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;-ms-overflow-style:scrollbar}@-ms-viewport{width:device-width}header,section{display:block}body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}h3,h5{margin-top:0;margin-bottom:.5rem}p{margin-top:0;margin-bottom:1rem}a{color:#007bff;text-decoration:none;background-color:transparent;-webkit-text-decoration-skip:objects}img{vertical-align:middle;border-style:none}a,button,input:not([type=range]),select{-ms-touch-action:manipulation;touch-action:manipulation}button{border-radius:0}button,input,select{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button,input{overflow:visible}button,select{text-transform:none}[type=submit],button,html [type=button]{-webkit-appearance:button}[type=button]::-moz-focus-inner,[type=submit]::-moz-focus-inner,button::-moz-focus-inner{padding:0;border-style:none}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}h3,h5{margin-bottom:.5rem;font-family:inherit;font-weight:500;line-height:1.2;color:inherit}h3{font-size:1.75rem}h5{font-size:1.25rem}.container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}@media (min-width:576px){.container{max-width:540px}}@media (min-width:768px){.container{max-width:720px}}@media (min-width:992px){.container{max-width:960px}}@media (min-width:1200px){.container{max-width:1140px}}.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.col-12,.col-md-3,.col-md-4,.col-md-8,.col-sm-12{position:relative;width:100%;min-height:1px;padding-right:15px;padding-left:15px}.col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}@media (min-width:576px){.col-sm-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}@media (min-width:768px){.col-md-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-md-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-md-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.offset-md-1{margin-left:8.333333%}}.form-control{display:block;width:100%;padding:.375rem .75rem;font-size:1rem;line-height:1.5;color:#495057;background-color:#fff;background-image:none;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem}.form-control::-ms-expand{background-color:transparent;border:0}.form-control::-webkit-input-placeholder{color:#868e96;opacity:1}.form-control:-ms-input-placeholder{color:#868e96;opacity:1}.form-control::-ms-input-placeholder{color:#868e96;opacity:1}select.form-control:not([size]):not([multiple]){height:calc(2.25rem + 2px)}.form-group{margin-bottom:1rem}.btn{display:inline-block;font-weight:400;text-align:center;white-space:nowrap;vertical-align:middle;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem}.btn-primary{color:#fff;background-color:#007bff;border-color:#007bff}.fade{opacity:0}.w-100{width:100%!important}.mb-0{margin-bottom:0!important}.text-right{text-align:right!important}.text-center{text-align:center!important}@media (min-width:576px){.text-sm-center{text-align:center!important}}@media (min-width:768px){.text-md-left{text-align:left!important}}.text-uppercase{text-transform:uppercase!important}.swiper-container{margin:0 auto;position:relative;overflow:hidden;z-index:1}.swiper-wrapper{position:relative;width:100%;height:100%;z-index:1;display:-webkit-box;display:-moz-box;display:-ms-flexbox;display:-webkit-flex;display:flex;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}.swiper-wrapper{-webkit-transform:translate3d(0,0,0);-moz-transform:translate3d(0,0,0);-o-transform:translate(0,0);-ms-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}.swiper-slide{-webkit-flex-shrink:0;-ms-flex:0 0 auto;flex-shrink:0;width:100%;height:100%;position:relative}@charset "UTF-8";@font-face{font-family:'FontAwesome';src:url(fonts/fontawesome-webfont.eot?v=4.7.0);src:url(fonts/fontawesome-webfont.eot?#iefix&v=4.7.0) format('embedded-opentype'),url(fonts/fontawesome-webfont.woff2?v=4.7.0) format('woff2'),url(fonts/fontawesome-webfont.woff?v=4.7.0) format('woff'),url(fonts/fontawesome-webfont.ttf?v=4.7.0) format('truetype'),url(fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular) format('svg');font-weight:400;font-style:normal}.fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.fa-phone:before{content:"\f095"}button::-moz-focus-inner{padding:0;border:0}.mt-10{margin-top:10px}.mb-0{margin-bottom:0}.mb-10{margin-bottom:10px}.pt-10{padding-top:10px}.pb-10{padding-bottom:10px}.mt-15{margin-top:15px}.fs-14{font-size:14px}.fs-16{font-size:16px}.fs-21{font-size:21px}@media only screen and (max-width:767px){.pt-xs-15{padding-top:15px}}@media only screen and (min-width:768px) and (max-width:949px){.pt-sm-15{padding-top:15px}.text-sm-center{text-align:center}}@font-face{font-family:'MontserratSemiBold';src:url(fonts/MontserratSemiBold.eot);src:url(fonts/MontserratSemiBold.eot) format('embedded-opentype'),url(fonts/MontserratSemiBold.woff2) format('woff2'),url(fonts/MontserratSemiBold.woff) format('woff'),url(fonts/MontserratSemiBold.ttf) format('truetype'),url(fonts/MontserratSemiBold.svg#MontserratSemiBold) format('svg');font-weight:400;font-style:normal}@font-face{font-family:'PlayFairDisplayRegular';src:url(fonts/PlayFairDisplayRegular.eot);src:url(fonts/PlayFairDisplayRegular.eot) format('embedded-opentype'),url(fonts/PlayFairDisplayRegular.woff2) format('woff2'),url(fonts/PlayFairDisplayRegular.woff) format('woff'),url(fonts/PlayFairDisplayRegular.ttf) format('truetype'),url(fonts/PlayFairDisplayRegular.svg#PlayFairDisplayRegular) format('svg');font-weight:400;font-style:normal}@font-face{font-family:'RobotoRegular';src:url(fonts/RobotoRegular.eot);src:url(fonts/RobotoRegular.eot) format('embedded-opentype'),url(fonts/RobotoRegular.woff2) format('woff2'),url(fonts/RobotoRegular.woff) format('woff'),url(fonts/RobotoRegular.ttf) format('truetype'),url(fonts/RobotoRegular.svg#RobotoRegular) format('svg')}body{font-family:'MontserratLight';font-size:14px;color:#3d383a}img{max-width:100%}a{text-decoration:none}.roboto{font-family:'RobotoRegular'}.site-header{position:absolute;top:0;left:0;width:100%;z-index:99}.banner-captions{top:100px;position:absolute;left:0;width:100%;z-index:99}.white-color{color:#fff}.phone-no{background:rgba(255,255,255,.85);padding:5px 10px;margin-right:-15px;color:#4e4e4e;font-family:'MontserratSemiBold';display:inline-block}.phone-no i{width:20px;height:20px;line-height:18px;display:inline-block;color:#ebb240;border:1px solid;border-radius:50%;text-align:center;font-size:12px}#form{border:1px solid #b9b9b9;background:rgba(255,255,255,.85)}h3,h5{font-family:'PlayFairDisplayRegular'}.form-title{font-family:'PlayFairDisplayRegular';display:block;width:100%;background:#61727e}.roboto{font-family:'RobotoRegular'}select.form-control:not([size]):not([multiple]){height:auto}.form-control{border:1px solid #909090;border-radius:0;padding:5px 10px;line-height:normal}select.form-control{padding:4px 10px}.form-holder{margin-top:-60px}.help-block.with-errors{font-size:12px;color:#a00}.btn-primary{border-radius:0;background:#ebb240;border-color:#ebb240;text-transform:uppercase}.sticky-btns{position:fixed;bottom:0;left:0;width:100%;z-index:999}.sticky-btns a{width:50%;float:left;height:36px;text-align:center;color:#fff;border-top:1px solid #fff;border-right:1px solid #fff;background-color:#ebb240;line-height:36px}.dummy-div{height:36px;width:100%}.sticky-btns a:last-child{border-right:none}.modal-header{background:#ebb240;border-radius:0}.modal-title{color:#fff}.modal-content{border-radius:0}@media only screen and (max-width:950px){.banner-captions{top:0;position:relative;left:0;width:100%;z-index:99;bottom:0}.white-bg-mobile{background:rgba(255,255,255,.87)}}</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body class="home">
<header class="site-header" id="masthead">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 text-center text-sm-center text-md-right">
				<img src="images/logo.png" alt="" class="mt-10 mb-10" />
			</div>
		</div>
	</div>
</header>
<section class="clearfix">
    <div id="bannerdeskslider" class="carousel slide d-none d-sm-none d-md-block d-lg-block w-100" data-ride="carousel">  
       <div class="carousel-inner">
            <div class="carousel-item active">
                 <img src="images/banner.jpg" alt="" class="w-100" />
				
            </div>
    <div class="carousel-item">
        <img src="images/bannerone.jpg" alt="" class="w-100" />
		
    </div>
    <div class="carousel-item">
      <img src="images/bannertwo.jpg" alt="" class="w-100" />
		
    </div>
	 <div class="carousel-item">
      <img src="images/bannerthree.jpg" alt="" class="w-100" />
		
    </div>
	 <div class="carousel-item">
         <img src="images/bannerfive.jpg" alt="" class="w-100" />
	 
    </div>
  </div>
  <a class="carousel-control-prev" href="#bannerdeskslider" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#bannerdeskslider" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
<div id="bannermobslider" class="carousel slide w-100 d-block d-sm-block d-md-none d-lg-none" data-ride="carousel">  
       <div class="carousel-inner">
            <div class="carousel-item active">
				<img src="images/banner-mob.jpg" class="w-100" alt=""  />
            </div>
    <div class="carousel-item">        
		<img src="images/bannerone-mob.jpg" class="w-100" alt="" />
    </div>
    <div class="carousel-item">     
		<img src="images/bannertwo-mob.jpg" class="w-100" alt="" />
    </div>
	 <div class="carousel-item">     
		<img src="images/bannerthree-mob.jpg" class="w-100" alt="" />
    </div>
	 <div class="carousel-item">     
		<img src="images/bannerfour-mob.jpg" class="w-100" alt=""  />
    </div>
	 <div class="carousel-item">       
	   <img src="images/bannerfive-mob.jpg" class="w-100" alt=""/>
    </div>
  </div>
  <a class="carousel-control-prev" href="#bannermobslider" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#bannermobslider" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
	<div class="banner-captions">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-8 caption-holder">
				</div>
				<div class="col-12 col-sm-12 col-md-3 offset-md-1 form-holder">
					<div class="row">
						<div id="form" class="col-12">
							<div class="row">
								<h1 class="mt-10 mb-10 text-center fs-19 white-color pt-10 pb-10 form-title text-uppercase">Connect with Us</h1>
							</div>
							<form action="contact.php" method="post" id="formbox" role="form" data-toggle="validator" onsubmit="return checkForm(this);">
								<div class="form-group">
									<input type="text" name="Name" class="form-control" placeholder="Name" required="" data-error="Please enter your Name" pattern="^[a-zA-Z\s]+$" />
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="text" name="Mobile" onkeypress="return isNumber(event)" class="form-control" placeholder="Phone" required="" data-error="Valid Number Please" id="Mobile" pattern="^\d{10}$" />
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="email" name="Email" class="form-control" placeholder="Email" required="" data-error="Vaild Email Please" />
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<select required="required" name="Project" class="form-control" data-error="Please select your Project">
										<option value="">Select Project</option>
										<option value="Blue">Blue, Whitefield from ₹1.47Cr onwards*</option>
										<option value="Codename Treasure Island">Treasure Island, Jalahalli from ₹65L*</option>
										<option value="Greenfield">Greenfield, Whitefield from ₹66L onwards*</option>
										<option value="Southern Crest">Southern Crest, JP Nagar from ₹1Cr+*</option>
										<option value="Codename Break Free">Code Breakfree E-City Phase 2 from ₹35L*</option>
										<option value="Codename Dil Chahta Hai">Dil Chahta Hai, E- City from ₹23L*</option>
										<option value="Luxor">Luxor, Hennur Main Road  from ₹72L*</option>
										<option value="Sameeksha">Sameeksha, Jalahalli from ₹28L*</option>
										<option value="Shriram Earth Off Mysore Road">Earth - Off Mysore Road from ₹20L*</option>
										<option value="Summitt">Summitt, E- City from ₹41L*</option>
									</select>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<button type="submit" id="submit" name="Submit" class="btn btn-primary w-100">Enquire</button>
								</div>
								<input type="hidden" class="form-url"  name="SourceURL" value="<?php echo $actual_link; ?>" />
							</form>
							
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="clearfix">
<img src="images/sec-scroll.jpg" alt="" class="d-none d-sm-none d-md-block d-lg-block w-100" />
<img src="images/sec-scroll-mob.jpg" alt="" class="d-block d-sm-block d-md-none d-lg-none w-100 mt-30" />
<div id="mobslider2" class="carousel slide w-100 d-block d-sm-block d-md-none d-lg-none" data-ride="carousel">  
       <div class="carousel-inner">
            <div class="carousel-item active">
				<img src="images/second-slide1.png" class="wd-200" alt=""  />
            </div>
    <div class="carousel-item">        
		<img src="images/second-slide2.png" class="wd-200" alt="" />
    </div>
    <div class="carousel-item">     
		<img src="images/second-slide3.png" class="wd-200" alt="" />
    </div>
  </div>
  <a class="carousel-control-prev" href="#mobslider2" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#mobslider2" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
<h2 class="d-block d-sm-block d-md-none d-lg-none" style="color:#007E94;margin: 0 auto; text-align: center;"> - Home starting <span style="color:#272555;">₹20L - ₹2Cr. across Bengaluru -</span></h2>
</section>
<section class="clearfix pt-45 pb-45">
	<div class="container">		
		<h2 class="fs-32 mt-0 mb-30 pb-10 text-center w-100 head-title">Projects</h2>
		<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'Allbangalore')" id="defaultOpen">All Bangalore</button>
  <button class="tablinks"  onclick="openCity(event, 'Prelaunches')">Ready-to-move-in Projects</button>
  <button class="tablinks" onclick="openCity(event, 'Northblr')">On-going Projects</button>
  <button class="tablinks" onclick="openCity(event, 'Newlaunches')">New Launches</button>
</div>
<div id="Allbangalore" class="tabcontent mt-xs-30">
   <div class="row project-row">
   <div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute"> Shriram Blue</div>
							<div class="around-border2">
							<div class="outer-div">
							   <div class="inner-divblue"></div>
							   </div>
							</div>
						</div>
						<div class="pd_eql">
							<div class="pb-30">
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Shriram Blue</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right"> ITPL, Whitefield</span></li>
									    <li><span>Configuration</span> <span class="float-right"> 2 & 3 bed condominiums</span> </li>
										<li><span> Price</span><span class="float-right">₹63 lakh</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmoreblue float-right">View</button>
                                   <p class="more moreblue wow slideInLeft" data-wow-delay="0.7s">Resort like living with weekday & weekend amenities</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								 <!--?php data-toggle="modal" data-target="#enquireNow" data-project="Chirping Woods Villaments">Enquire</button>?-->
							</div>
						
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute"> Codename Treasure Island</div>
							<div class="around-border2">
							<div class="outer-div">
							   <div class="inner-divtreasure"></div>
							   </div>								
							</div>
							
						</div>
						<div class="pd_eql">
							<div class="pb-30">
							<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Codename Treasure Island</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right"> Jalahalli</span></li>
									    <li><span>Configuration</span> <span class="float-right"> Spacious 2 bed homes</span> </li>
										<li><span> Price</span><span class="float-right">₹61 lakh</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmorecode float-right">View</button>
                                   <p class="more morecode wow slideInLeft" data-wow-delay="0.7s">Boutique community with just 152 families</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								
								<!--a class="btn btn-primary w-100" href="javascript:void(0)" data-toggle="modal" data-target="#enquireNow" data-project="Chirping Woods Villaments">Enquire</a-->
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute"> Shriram Greenfield</div>
							<div class="around-border2">
								<div class="outer-div">
							   <div class="inner-divgreen"></div>
							   </div>
							</div>
						</div>
						<div class="pd_eql">
							<div class="pb-30">
								
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Shriram Greenfield</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right"> Budigere Cross, off Whitefield</span></li>
									    <li><span>Configuration</span> <span class="float-right"> 2, 2.5 & 3 bed homes</span> </li>
										<li><span> Price</span><span class="float-right">₹40 lakh</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmoregreen float-right">View</button>
                                   <p class="more moregreen wow slideInLeft" data-wow-delay="0.7s">40-acre mega township with residential, retail, healthcare & educational facilities</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								 <!--?php href="javascript:void(0)" data-toggle="modal" data-target="#enquireNow" data-project="Chirping Woods Apartments">Enquire</a> ?-->
							
						</div>
					</div>
				</div>
			</div>
			
		</div>
<div class="row project-row">
		<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">
					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute"> Shriram Southern Crest</div>
							<div class="around-border2">
								<div class="outer-div">
							     <div class="inner-divsouth"></div>
								</div>
							</div>
							
						</div>
						<div class="pd_eql">
							<div class="pb-30">
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Shriram Southern Crest</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right"> Sarakki signal, JP Nagar </span></li>
									    <li><span>Configuration</span> <span class="float-right">Convertible 2.5 bed homes</span> </li>
										<li><span> Price</span><span class="float-right">₹92 lakh</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmoresouth float-right">View</button>
                                   <p class="more moresouth wow slideInLeft" data-wow-delay="0.7s">Lavish lifestyle with a 14,000 sft clubhous</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								<!--a class="btn btn-primary w-100" href="javascript:void(0)" data-toggle="modal" data-target="#enquireNow" data-project="Chirping Woods Apartments">Enquire</a-->
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">
					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute">Codename Breakfree</div>
							<div class="around-border2">
								<div class="outer-div">
							   <div class="inner-divbreak"></div>
							</div></div>
						</div>
						<div class="pd_eql">
							<div class="pb-30">
								
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Codename Breakfree</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right"> E-City Ph-2</span></li>
									    <li><span>Configuration</span> <span class="float-right">Trendy 2 & 3 bed home</span> </li>
										<li><span> Price</span><span class="float-right">₹35 lakhs</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmorebreak float-right">View</button>
                                   <p class="more morebreak wow slideInLeft" data-wow-delay="0.7s">Sprawling 11,000 sft clubhouse with enviable amenities.</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								</div>
						
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">
					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute">Codename Dil Chahta Hai</div>
							<div class="around-border2">
							<div class="outer-div">
							   <div class="inner-divdch"></div>
							</div>
							</div>
							
						</div>
						<div class="pd_eql">
							<div class="pb-30">								
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Codename Dil Chahta Hai</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right">off Hosur road</span></li>
									    <li><span>Configuration</span> <span class="float-right">Smart 2 BHK</span> </li>
										<li><span> Price</span><span class="float-right">₹23 lakhs</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmorecode float-right">View</button>
                                   <p class="more morecode wow slideInLeft" data-wow-delay="0.7s">40+ lifestyle amenities – clubhouse, lakeside promenade, themed gardens, lush green gardens & more.</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								
						</div>
					</div>
				</div>
			</div>
			
			
		</div>

<div class="row project-row">
		<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">
					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute">Shriram Luxor</div>
							<div class="around-border">
								<div class="outer-div">
							   <div class="inner-divluxor"></div>
							</div>
								<h4 class="mt-0 mb-0 project-title fs-24"><img src="images/luxor-logo.png" alt="" /></h4>
							</div>
							
						</div>
						<div class="pd_eql">
							<div class="pb-30">
								
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Shriram Luxor</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right">Hennur Main road</span></li>
									    <li><span>Configuration</span> <span class="float-right">2 & 2.5 bed homes</span> </li>
										<li><span> Price</span><span class="float-right">₹63 lakhs</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmoreluxor float-right">View</button>
                                   <p class="more moreluxor wow slideInLeft" data-wow-delay="0.7s"> 22,000 sft grand clubhouse for just 444 families.</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								</div>
						
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">
					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute">Shriram Sameeksha</div>
							<div class="around-border2">
							<div class="outer-div">
							   <div class="inner-divsameek"></div>
							</div>					
							</div>
							
						</div>
						<div class="pd_eql">
							<div class="pb-30">
															
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Shriram Sameeksha</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right">Jalahalli</span></li>
									    <li><span>Configuration</span> <span class="float-right">1 & 2 BHK</span> </li>
										<li><span> Price</span><span class="float-right">₹28 lakhs</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmoresameek float-right">View</button>
                                   <p class="more moresameek wow slideInLeft" data-wow-delay="0.7s"> Grand clubhouse with swimming pool, amphitheatre, multipurpose hall & more.</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
									</div>
						
					</div>
				</div>
			</div>
		<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">
					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute">Shriram Earth - Off Mysore Road</div>
							<div class="around-border2">
								<div class="outer-div">
							   <div class="inner-divearth"></div>
							</div>
							</div>
						</div>
						<div class="pd_eql">
							<div class="pb-30">
								
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Shriram Earth - Off Mysore Road</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right">Dodda Alada Mara</span></li>
									    <li><span>Configuration</span> <span class="float-right">1 & 2 BHK</span> </li>
										<li><span> Price</span><span class="float-right">₹20 lakhs</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmorearth float-right">View</button>
                                   <p class="more morearth wow slideInLeft" data-wow-delay="0.7s"> 20 acres of pristine paradise with 381 well-planned plots.</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								</div>
						
					</div>
				</div>
			</div>
			
		
		</div>
		<div class="row project-row">
		<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">
					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute">Shriram Summitt</div>
							<div class="around-border2">
								<div class="outer-div">
							   <div class="inner-divsummitt"></div>
							</div>
								
							</div>
							
						</div>
						<div class="pd_eql">
							<div class="pb-30">
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Shriram Summitt</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right">Wipro back gate, E-City Ph-1</span></li>
									    <li><span>Configuration</span> <span class="float-right">1, 2 & 2.5 bed homes</span> </li>
										<li><span> Price</span><span class="float-right">₹37 lakhs</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmoresummitt float-right">View</button>
                                   <p class="more moresummitt wow slideInLeft" data-wow-delay="0.7s"> 9.5 acres of open space in a 14 acre project.</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								</div>
						</div>
					
				</div>
			</div>
		</div>
</div>
<div id="Prelaunches" class="tabcontent">
<div class="row project-row">
	<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">
					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute">Shriram Summitt</div>
							<div class="around-border2">
								<div class="outer-div">
							   <div class="inner-divsummitt"></div>
							</div>
								
							</div>
							
						</div>
						<div class="pd_eql">
							<div class="pb-30">
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Shriram Summitt</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right">Wipro back gate, E-City Ph-1</span></li>
									    <li><span>Configuration</span> <span class="float-right">1, 2 & 2.5 bed homes</span> </li>
										<li><span> Price</span><span class="float-right">₹37 lakhs</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmoresummitt float-right">View</button>
                                   <p class="more moresummitt wow slideInLeft" data-wow-delay="0.7s"> 9.5 acres of open space in a 14 acre project.</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								</div>
						</div>
					
				</div>
			</div>
			<div class="clr"></div>
		<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">
					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute">Shriram Luxor</div>
							<div class="around-border">
								<div class="outer-div">
							   <div class="inner-divluxor"></div>
							</div>
								<h4 class="mt-0 mb-0 project-title fs-24"><img src="images/luxor-logo.png" alt="" /></h4>
							</div>
							
						</div>
						<div class="pd_eql">
							<div class="pb-30">
								
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Shriram Luxor</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right">Hennur Main road</span></li>
									    <li><span>Configuration</span> <span class="float-right">2 & 2.5 bed homes</span> </li>
										<li><span> Price</span><span class="float-right">₹63 lakhs</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmoreluxor float-right">View</button>
                                   <p class="more moreluxor wow slideInLeft" data-wow-delay="0.7s"> 22,000 sft grand clubhouse for just 444 families.</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								</div>
						
					</div>
				</div>
			</div>
			
			<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">
					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute"> Shriram Southern Crest</div>
							<div class="around-border2">
								<div class="outer-div">
							     <div class="inner-divsouth"></div>
								</div>
							</div>
							
						</div>
						<div class="pd_eql">
							<div class="pb-30">
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Shriram Southern Crest</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right"> Sarakki signal, JP Nagar </span></li>
									    <li><span>Configuration</span> <span class="float-right">Convertible 2.5 bed homes</span> </li>
										<li><span> Price</span><span class="float-right">₹92 lakh</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmoresouth float-right">View</button>
                                   <p class="more moresouth wow slideInLeft" data-wow-delay="0.7s">Lavish lifestyle with a 14,000 sft clubhous</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								<!--a class="btn btn-primary w-100" href="javascript:void(0)" data-toggle="modal" data-target="#enquireNow" data-project="Chirping Woods Apartments">Enquire</a-->
							
						</div>
					</div>
				</div>
			</div>
		</div>
</div>

<div id="Northblr" class="tabcontent">
<div class="row project-row">
		
		<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute"> Shriram Blue</div>
							<div class="around-border2">
							<div class="outer-div">
							   <div class="inner-divblue"></div>
							   </div>
							</div>
						</div>
						<div class="pd_eql">
							<div class="pb-30">
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Shriram Blue</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right"> ITPL, Whitefield</span></li>
									    <li><span>Configuration</span> <span class="float-right"> 2 & 3 bed condominiums</span> </li>
										<li><span> Price</span><span class="float-right">₹63 lakh</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmoreblue float-right">View</button>
                                   <p class="more moreblue wow slideInLeft" data-wow-delay="0.7s">Resort like living with weekday & weekend amenities</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								 <!--?php data-toggle="modal" data-target="#enquireNow" data-project="Chirping Woods Villaments">Enquire</button>?-->
							</div>
						
					</div>
				</div>
			</div>
		<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute"> Codename Treasure Island</div>
							<div class="around-border2">
							<div class="outer-div">
							   <div class="inner-divtreasure"></div>
							   </div>								
							</div>
							
						</div>
						<div class="pd_eql">
							<div class="pb-30">
							<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Codename Treasure Island</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right"> Jalahalli</span></li>
									    <li><span>Configuration</span> <span class="float-right"> Spacious 2 bed homes</span> </li>
										<li><span> Price</span><span class="float-right">₹61 lakh</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmorecode float-right">View</button>
                                   <p class="more morecode wow slideInLeft" data-wow-delay="0.7s">Boutique community with just 152 families</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								
								<!--a class="btn btn-primary w-100" href="javascript:void(0)" data-toggle="modal" data-target="#enquireNow" data-project="Chirping Woods Villaments">Enquire</a-->
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">
					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute">Codename Dil Chahta Hai</div>
							<div class="around-border2">
							<div class="outer-div">
							   <div class="inner-divdch"></div>
							</div>
							</div>
							
						</div>
						<div class="pd_eql">
							<div class="pb-30">								
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Codename Dil Chahta Hai</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right">off Hosur road</span></li>
									    <li><span>Configuration</span> <span class="float-right">Smart 2 BHK</span> </li>
										<li><span> Price</span><span class="float-right">₹23 lakhs</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmorecode float-right">View</button>
                                   <p class="more morecode wow slideInLeft" data-wow-delay="0.7s">40+ lifestyle amenities – clubhouse, lakeside promenade, themed gardens, lush green gardens & more.</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								
						</div>
					</div>
				</div>
			</div>
			
		</div>
		</div>
<div id="Newlaunches" class="tabcontent">
		<div class="row project-row">
		<div class="col-12 col-sm-12 col-md-2 mb-30"></div>
		
			<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">
					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute">Shriram Sameeksha</div>
							<div class="around-border2">
							<div class="outer-div">
							   <div class="inner-divsameek"></div>
							</div>					
							</div>
							
						</div>
						<div class="pd_eql">
							<div class="pb-30">
															
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Shriram Sameeksha</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right">Jalahalli</span></li>
									    <li><span>Configuration</span> <span class="float-right">1 & 2 BHK</span> </li>
										<li><span> Price</span><span class="float-right">₹28 lakhs</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmoresameek float-right">View</button>
                                   <p class="more moresameek wow slideInLeft" data-wow-delay="0.7s"> Grand clubhouse with swimming pool, amphitheatre, multipurpose hall & more.</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
									</div>
						
					</div>
				</div>
			</div>
		<div class="col-12 col-sm-12 col-md-4 mb-30">
				<div class="clearfix project-holder">
					
						<div class="w-100 align-self-start">
						<div class="nameproj position-absolute">Codename Breakfree</div>
							<div class="around-border2">
								<div class="outer-div">
							   <div class="inner-divbreak"></div>
							</div></div>
						</div>
						<div class="pd_eql">
							<div class="pb-30">
								
								<div class="same-height-div">
									<h5 class="mt-15 mb-15 fs-25 fs-xs-20 clr-ble MontserratLight">Codename Breakfree</h5>
									<ul class="custom-bullets MontserratLight list-unstyled">
									    <li><span>Location</span> <span class="float-right"> E-City Ph-2</span></li>
									    <li><span>Configuration</span> <span class="float-right">Trendy 2 & 3 bed home</span> </li>
										<li><span> Price</span><span class="float-right">₹35 lakhs</span></li>
										<li><span>About the Project</span> <button class="viewmore viewmorebreak float-right">View</button>
                                   <p class="more morebreak wow slideInLeft" data-wow-delay="0.7s">Sprawling 11,000 sft clubhouse with enviable amenities.</p></li>
										<li><span> More Details</span><span class="float-right"> <a href="#formbox" class="scroll enqu w-100" id="anch_blue">Enquire</a></span></li>
									</ul>
								</div>
								</div>
						
					</div>
				</div>
			</div>
					<div class="col-12 col-sm-12 col-md-2 mb-30"></div>
		</div>
		</div>
		<div class="clr"></div>		
	</div>
</section>

<footer class="site-footer">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<p><a href="privacypolicy.php" target="_blank"> Privacy Policy</a>, Terms &amp; Conditions Apply*</p>
				<p>&copy; Copyright <?php echo date('Y'); ?>, All rights reserved with Shriram Properties Limited.</p>
				
				</div>
		</div>
	</div>
</footer>
<div class="sticky-btns d-block d-sm-block d-md-none d-lg-none">
	<a href="#formbox" class="scroll col-sm-6">Enquire</a><a href="tel:08040831333" class="col-sm-6">Call Us</a>
	<!--a href="tel:+918040831333">Call</a-->
</div>
<div class="dummy-div d-block d-sm-block d-md-none d-lg-none"></div>
<script type="text/javascript" src="js/jquery.min.js" defer></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" defer></script>
<script type="text/javascript" src="js/bootstrap.min.js" defer></script>
<script type="text/javascript" src="js/swiper.min.js" defer></script>
<script type="text/javascript" src="js/wow.min.js" defer></script>
<script type="text/javascript" src="js/validator.min.js" defer></script>
<script type="text/javascript" src="js/custom.js" defer></script>
<script type="text/javascript">
new WOW().init(); </script>

 <script type="text/javascript">
  function checkForm(form)
  {     form.Submit.disabled = true;
    return true;
  }

</script>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
document.getElementById("defaultOpen").click();
</script>
<noscript id="deferred-styles">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/swiper.min.css" rel="stylesheet" type="text/css" />
<link href="css/animate.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="css/magnific-popup.css" rel="stylesheet" type="text/css" />
<link href="css/padd-mar.css" rel="stylesheet" type="text/css" />
<link href="style.css" rel="stylesheet" type="text/css" />
</noscript>
<script>
$(document).ready(function(){
  $("button.viewmoreblue").click(function(){
    $(".moreblue").toggle();
  }); 
});
</script>
<script>
$(document).ready(function(){
$("button.viewmorecode").click(function(){
    $(".morecode").toggle();
  });
});
</script>
<script>
$(document).ready(function(){
 $("button.viewmoregreen").click(function(){
    $(".moregreen").toggle();
  });
});
</script>
<script>
$(document).ready(function(){
 $("button.viewmorearth").click(function(){
    $(".morearth").toggle();
  });
});
</script>
<script>
$(document).ready(function(){
 $("button.viewmoreluxor").click(function(){
    $(".moreluxor").toggle();
  });
});
</script>
<script>
$(document).ready(function(){
$("button.viewmoresouth").click(function(){
    $(".moresouth").toggle();
  });
});
</script>
<script>
$(document).ready(function(){
$("button.viewmorebreak").click(function(){
    $(".morebreak").toggle();
  });
});
</script>
<script>
$(document).ready(function(){
$("button.viewmoresameek").click(function(){
    $(".moresameek").toggle();
  });
});
</script>
<script>
$(document).ready(function(){
$("button.viewmoresummitt").click(function(){
    $(".moresummitt").toggle();
  });
});
</script>
<script>
  var loadDeferredStyles = function() {
	var addStylesNode = document.getElementById("deferred-styles");
	var replacement = document.createElement("div");
	replacement.innerHTML = addStylesNode.textContent;
	document.body.appendChild(replacement)
	addStylesNode.parentElement.removeChild(addStylesNode);
  };
  var raf = requestAnimationFrame || mozRequestAnimationFrame ||
	  webkitRequestAnimationFrame || msRequestAnimationFrame;
  if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
  else window.addEventListener('load', loadDeferredStyles);
</script>


</body>
</html>